package com.example.notescategories;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import java.util.ArrayList;

public class ArrayAdapterN<image> extends android.widget.ArrayAdapter<Nota> {
    private Context mContext;

    public ArrayAdapterN(Context context, int resource, ArrayList<Nota> toDoItems) {
        super(context,R.layout.list_item, toDoItems);
        this.mContext = context;

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final String todo=getItem(position).getTesto();
        final String start=getItem(position).getCreateOnString();


        if(convertView==null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.list_item, parent, false);
        }

        final TextView tvCategory= convertView.findViewById(R.id.TextViewC);
        final TextView tvTodo= convertView.findViewById(R.id.TextViewT);
        final TextView tvStart= convertView.findViewById(R.id.TextViewD);

        String category= getItem(position).getCategoria();

        tvTodo.setText(todo);
        tvStart.setText(start);
        tvCategory.setText(category);

        return convertView;
    }
}