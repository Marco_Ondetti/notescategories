package com.example.notescategories;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.io.ByteArrayOutputStream;

public class AggiungiN extends AppCompatActivity {

    private ListView categoria;
    private EditText editText;
    private Button bottonen;
    private ListaNote note;
    private ArrayAdapter<String> listViewAdapter;
    private ArrayAdapter<String> spinnerAdapter;
    private ImageView imageview;
    private String Categoria;
    private Button Camera;
    byte[] imagedata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.agiungi_n);

        imageview=findViewById(R.id.imageView_note);
        Toolbar toolbar = findViewById(R.id.toolbar);
        categoria =findViewById(R.id.valori);
        bottonen =findViewById(R.id.confirmN);
        Camera =findViewById(R.id.Camera);
        editText= findViewById(R.id.TextN);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Aggiungi la nota");

        if(ContextCompat.checkSelfPermission(AggiungiN.this,Manifest.permission.CAMERA)!=
        PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(AggiungiN.this,new String[]{
                    Manifest.permission.CAMERA
            },100);
        }
        Camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,100);
            }
        });


        ListView lv=(ListView) findViewById(R.id.valori);
        listViewAdapter=new ArrayAdapter<String>(this,R.layout.riga);
        lv.setAdapter(listViewAdapter);
        note = ListaNote.getInstance();

        spinnerAdapter=new ArrayAdapter<String>(this, R.layout.riga);
        Log.d("key", note.getElemento().toString());
        spinnerAdapter.addAll(note.getElemento());
        final Spinner sp=(Spinner) findViewById(R.id.Elementi);
        sp.setAdapter(spinnerAdapter);
        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Categoria = sp.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        bottonen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent resultIntent = new Intent();
                resultIntent.putExtra("TODO_TASK", editText.getText().toString());
                resultIntent.putExtra("CATEGORY", Categoria);
                resultIntent.putExtra("IMAGE", imagedata);
                setResult(RESULT_OK, resultIntent);
                finish();
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode,int resultCode,Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==100){
            Bitmap captureImage=(Bitmap) data.getExtras().get("data");
            imageview.setImageBitmap(captureImage);
            ByteArrayOutputStream baos=new ByteArrayOutputStream();
            captureImage.compress(Bitmap.CompressFormat.JPEG,100,baos);
            imagedata=baos.toByteArray();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        Toast.makeText(this, "Nota non aggiunta", Toast.LENGTH_LONG).show();
        Intent resultIntent = new Intent(this,MainActivity.class);
        startActivity(resultIntent);
        return true;
    }

}
