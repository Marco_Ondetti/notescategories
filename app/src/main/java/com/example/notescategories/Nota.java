package com.example.notescategories;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Locale;

public class Nota {

    private String testo;
    private String categoria;
    private GregorianCalendar data;
    private String datas;
    private int id;


    public Nota(String todo){
        super();
        id=0;
        this.testo = todo;
        this.data = new GregorianCalendar();
        this.categoria= "";
        datas =new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).format(data.getTime());
    }

    public byte[] image;

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public void setTesto(String testo) {
        this.testo = testo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public void setData(GregorianCalendar data) {
        this.data = data;
    }

    public GregorianCalendar getData() {
        return data;
    }

    public String getCreateOnString() {  return datas; }

    public String getTesto() {
        return testo;
    }

    @Override
    public String toString() {
        String ritorno= "Data Inizio: "+ datas +" TODO: "+ testo +" Categoria: "+categoria;
        return ritorno;
    }

}
