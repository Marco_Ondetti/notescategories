package com.example.notescategories;

import android.database.Cursor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

public class ListaNote {

    boolean found;
    private HashMap <String, ArrayList<String>> list=new HashMap<String, ArrayList<String>>();

    private static ListaNote instance = new ListaNote();

    public ListaNote(){ //All deve sempre essere presente nella lista di elementi
        addElemento("All" );
    }

    public static ListaNote getInstance(){
        return instance;
    }

    public  void refreshCategories(DatabaseHelper mydb){
        Cursor result = mydb.getAllCategories();
        if (result.getCount() <= 0) {

        } else {
            while (result.moveToNext()) {
                String category = result.getString(1);
                addElemento(category);
            }
        }
    }

    public boolean addElemento(String elemento){
        Set<String> keySet = list.keySet();
        boolean temp=true;
        for(String key:keySet){
            if(key.equals(elemento)){
                temp=false;
            }
        }

        if(temp==true){
            list.put(elemento,null);
            return true;
        }
        if(temp==false){
            return false;
        }
        return false;
    }

    public void removeElemento(String elemento){

        Set<String> keySet = list.keySet();
        for(String key:keySet){
            if(key.equals(elemento)){
                found=true;
            }

        }

        if(elemento.equals("All")){
            found=false;
        }

        if(found==true){
            found=false;
            list.remove(elemento);
        }
    }

    public Collection<String> getElemento(){
        return list.keySet();
    }

}
