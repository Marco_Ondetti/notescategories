package com.example.notescategories;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class AggiungiC extends AppCompatActivity {
    private Button bottonec;
    private EditText categoriesEditText;
    private ListaNote listanote;
    private Context AggiungiCContext;
    private String ok;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AggiungiCContext =this.getApplicationContext();
        listanote = ListaNote.getInstance();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aggiungi_c);
        bottonec=(Button) findViewById(R.id.confirmC);
        categoriesEditText=(EditText) findViewById(R.id.EditTextC);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Aggiungi la categoria");
        ok="false";
        bottonec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!categoriesEditText.getText().toString().trim().equals("")) {
                    addCategory(categoriesEditText.getText().toString());
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra("CATEGORY", categoriesEditText.getText().toString());
                    resultIntent.putExtra("OK", ok);
                    setResult(RESULT_OK, resultIntent);
                    finish();
                }
                else{
                    Toast.makeText(AggiungiCContext, "Categoria vuota", Toast.LENGTH_LONG).show();
                }
            }
        });
    }


   public boolean addCategory(String category){

       listanote = ListaNote.getInstance();

       boolean risultato= listanote.addElemento(category);
       if(risultato==true){
           Toast.makeText(this, "Categoria: "+category+" aggiunta con successo", Toast.LENGTH_LONG).show();
           ok="true";
           return true;
       }
       else{
           Toast.makeText(this, "Categoria: "+category+" esiste gia", Toast.LENGTH_LONG).show();
           ok="false";
           return false;
       }

   }

    @Override
    public boolean onSupportNavigateUp() {
        Toast.makeText(this, "Categoria non aggiunta", Toast.LENGTH_LONG).show();
        Intent resultIntent = new Intent(this,MainActivity.class);
        startActivity(resultIntent);
        return true;
    }
}
