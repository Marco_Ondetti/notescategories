package com.example.notescategories;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class VisualizzaN extends AppCompatActivity {

    ImageView imageView;
    TextView textView;
    String test;
    byte[] imagearray;
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.visualizza_n);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Visualizza la nota");

        imageView=findViewById(R.id.immaginenota);
        textView=findViewById(R.id.Testonota);
        test = getIntent().getExtras().getString("TODO");
        imagearray= getIntent().getByteArrayExtra("IMAGE");
        if(imagearray!=null) {
            bitmap = BitmapFactory.decodeByteArray(imagearray, 0, imagearray.length);
            imageView.setImageBitmap(bitmap);
        }
        textView.setText(test);

    }

    public boolean onSupportNavigateUp() {
        Intent resultIntent = new Intent(this,MainActivity.class);
        startActivity(resultIntent);
        return true;
    }
}
