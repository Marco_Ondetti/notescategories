package com.example.notescategories;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

public class MainActivity extends AppCompatActivity {
    static final int DELETE_CATEGORIES_REQUEST = 3;
    static final int ADD_CATEGORIES_REQUEST = 2;
    static final int ADD_NOTES_REQUEST = 1;
    ArrayList<Nota> note;
    private Button deleteButton;
    private android.widget.ArrayAdapter listViewAdapter;
    private android.widget.ArrayAdapter spinnerAdapter;
    ArrayAdapterN adapter;
    static DatabaseHelper mydb;
    private ListaNote listanote;
    private Context mainContext;
    private static final String TAG = "MainActivity";
    private String Categoria;
    String ok="";
    Cursor result;
    Spinner sp;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        mydb = new DatabaseHelper(this);
        mainContext=this.getApplicationContext();


        final ListView listView = findViewById(R.id.listView);
        deleteButton = findViewById(R.id.deleteButton);

        note = new ArrayList<Nota>();

        adapter = new ArrayAdapterN(MainActivity.this, R.layout.list_item, note);
        ListView lv=(ListView) findViewById(R.id.valorimostra);

        sp=(Spinner) findViewById(R.id.spinnerc);
        spinnerAdapter=new android.widget.ArrayAdapter(this, R.layout.riga);
        spinnerAdapter.clear();
        spinnerAdapter.notifyDataSetChanged();

        listViewAdapter=new android.widget.ArrayAdapter(this,R.layout.riga);


        lv.setAdapter(listViewAdapter);
        listView.setAdapter(adapter);

        listanote = ListaNote.getInstance();
        listanote.refreshCategories(mydb);
        adapter.notifyDataSetChanged();

        spinnerAdapter.clear();
        spinnerAdapter.addAll(listanote.getElemento());
        spinnerAdapter.notifyDataSetChanged();
        sp.setAdapter(spinnerAdapter);



        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                note.clear();
                Categoria =sp.getSelectedItem().toString();

                    if(Categoria.equals("All")){
                        result = mydb.getAllData();
                    }
                    else {
                        result = mydb.getDataFromCategory(Categoria);
                    }

                    if (result.getCount() <= 0) {
                        note.clear();
                        adapter.notifyDataSetChanged();

                    } else {

                        while (result.moveToNext()) {
                            int id2 = result.getInt(0);
                            String createline = result.getString(2);
                            String todo = result.getString(1);
                            String Category = result.getString(4);
                            byte[] Image = result.getBlob(5);

                            DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                            Date date = null;
                            try {
                                date = format.parse(createline);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            GregorianCalendar calendar = new GregorianCalendar();
                            calendar.setTime(date);

                            Nota dbTodo = new Nota(result.getString(1));
                            dbTodo.setData(calendar);
                            dbTodo.setCategoria(Category);
                            dbTodo.setImage(Image);
                            note.add(0, dbTodo);
                            adapter.notifyDataSetChanged();

                        }
                    }
                }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mydb.deleteAll();
                note.clear();
                adapter.notifyDataSetChanged();

            }
        });


        registerForContextMenu(listView);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int pos, long id) {

                int idt= note.get(pos).getId();
                String todo= note.get(pos).getTesto();
                String categoria= note.get(pos).getCategoria();
                Intent modifyIntent= new Intent(mainContext, ModificaN.class);
                modifyIntent.putExtra("ID", idt );
                modifyIntent.putExtra("CATEGORIA", categoria );
                modifyIntent.putExtra("TODO", todo );
                startActivity(modifyIntent);
                return true;
            }
        });

        final SwipeDetector swipeDetector=new SwipeDetector();
        listView.setOnTouchListener(swipeDetector);

        AdapterView.OnItemClickListener listener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                if (swipeDetector.swipeDetected()) {
                    if (swipeDetector.getAction() == SwipeDetector.Action.LR) {

                        String todo= note.get(pos).getTesto();
                        String createon= note.get(pos).getCreateOnString();
                        String category= note.get(pos).getCategoria();
                        mydb.deletePos(todo,createon,category);
                        refresh();
                    }
                    else{
                        Log.d("swipe","da destra a sinistra");
                        String todo= note.get(pos).getTesto();
                        byte[] image= note.get(pos).getImage();
                        Intent modifyIntent= new Intent(mainContext, VisualizzaN.class);
                        modifyIntent.putExtra("IMAGE", image );
                        modifyIntent.putExtra("TODO", todo );
                        startActivity(modifyIntent);
                    }
                }
            }
        };
        listView.setOnItemClickListener(listener);

    }


    public void refresh(){
        this.recreate();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_aggiungi_n) {
            Intent i = new Intent(getApplicationContext(), AggiungiN.class);
            startActivityForResult(i, ADD_NOTES_REQUEST);
            return true;

        } else if (id == R.id.action_agiiungi_c) {
            Intent i = new Intent(getApplicationContext(), AggiungiC.class);
            startActivityForResult(i, ADD_CATEGORIES_REQUEST);
            return true;

        } else if (id == R.id.action_exit) {
            Intent homeIntent = new Intent(Intent.ACTION_MAIN);
            homeIntent.addCategory( Intent.CATEGORY_HOME );
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homeIntent);
        }
        else if (id == R.id.action_cancella_c) {
            Intent i = new Intent(this, CancellaC.class);
            startActivityForResult(i, DELETE_CATEGORIES_REQUEST);
            return true;

        }
        else if(id==R.id.action_ricarica){
            refresh();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_NOTES_REQUEST) {

            if (resultCode == RESULT_OK) {
                String returnValue = data.getStringExtra("TODO_TASK");
                String category = data.getStringExtra("CATEGORY");
                byte[] image = data.getByteArrayExtra("IMAGE");

                Log.d(MainActivity.class.getName(), "onActivityResult() -> " + returnValue);

                if (returnValue != null) {
                    addNewItem(returnValue, category, image);
                    adapter.notifyDataSetChanged();
                    refresh();
                }
            }
        }

        if (requestCode == ADD_CATEGORIES_REQUEST) {

            if (resultCode == RESULT_OK) {
                ok = data.getStringExtra("OK");
                if (ok.equals("true")) {
                    String category = data.getStringExtra("CATEGORY");
                    addNewCategory(category);
                    refresh();
                }
            } else {

            }
            adapter.notifyDataSetChanged();
        }


        if (requestCode == DELETE_CATEGORIES_REQUEST) {

            if (resultCode == RESULT_OK) {
                String Category= data.getStringExtra("CATEGORY");
                if(Category.equals("All")){
                    Toast.makeText(getApplicationContext(),
                            "Categoria 'All' non cancellabile", Toast.LENGTH_LONG).show();
                }
                else {
                    deleteCategory(Category);
                }

            }
            adapter.notifyDataSetChanged();
        }

    }

    private void addNewItem(String todo,String categoria,byte[] image) {

        if (todo.length() == 0) {
            Toast.makeText(getApplicationContext(),"Nota senza contenuto", Toast.LENGTH_LONG).show();
            return;
        }

        Nota newTodo = new Nota(todo);
        newTodo.setCategoria(categoria);
        newTodo.setImage(image);
        note.add(0,newTodo);

        boolean inserito = mydb.insertData(newTodo);

        if (inserito == true) {
            Toast.makeText(MainActivity.this, "Nota inserita", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(MainActivity.this, "ERRORE non inserito nel DB", Toast.LENGTH_LONG).show();
        }
        adapter.notifyDataSetChanged();
    }

    private void addNewCategory(String category) {
        boolean inserito = mydb.insertCategory(category);
    }

    private void deleteCategory(String category) {

        if(!category.equals("All")) {
            mydb.deleteCategory(category);
            listanote.removeElemento(category);
        }

        refresh();
        spinnerAdapter.notifyDataSetChanged();
        adapter.notifyDataSetChanged();
        Toast.makeText(mainContext, "Categoria: "+ Categoria +" cancellata", Toast.LENGTH_LONG).show();
    }

    public static DatabaseHelper getMyDb(){
        return mydb;
    }

}



