package com.example.notescategories;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME= "notes.db";
    public static final String TABLE_NAME = "note_table";
    public static final String TABLE_NAME2 = "categories_table";
    public static final String ID ="id";
    public static final String MEX = "MEX";
    public static final String DAT = "DAT";
    public static final String CATEGORY="CATEGORY";
    public static final String IMAGE="IMAGE";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 2);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE "+TABLE_NAME+" (ID INTEGER PRIMARY KEY AUTOINCREMENT, MEX VARCHAR(20) NOT NULL, DAT VARCHAR(20),CATEGORY TEXT,IMAGE BLOB)");
        db.execSQL("CREATE TABLE "+TABLE_NAME2+" (ID INTEGER PRIMARY KEY AUTOINCREMENT, CATEGORY VARCHAR(20))");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME2);
        onCreate(db);
    }

    public boolean insertData(Nota item){
        SQLiteDatabase db= this.getWritableDatabase();
        ContentValues contentValues= new ContentValues();
        contentValues.put(MEX,item.getTesto());
        String dat=item.getCreateOnString();
        contentValues.put(DAT,dat);
        contentValues.put(CATEGORY,item.getCategoria());
        contentValues.put(IMAGE,item.getImage());

        long result= db.insert(TABLE_NAME,null,contentValues);

        if(result==-1){
            return false;
        }
        else{
            return true;
        }
    }

    public boolean insertCategory(String category){
        SQLiteDatabase db= this.getWritableDatabase();
        ContentValues contentValues= new ContentValues();
        contentValues.put(CATEGORY,category);

        long result= db.insert(TABLE_NAME2,null,contentValues);

        if(result==-1){
            return false;
        }
        else{
            return true;
        }
    }

    public void deleteAll(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME,null,null);
        db.execSQL("delete from "+ TABLE_NAME);
    }

    public void deleteCategory(String category){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("UPDATE "+ TABLE_NAME+ " SET "+CATEGORY+"='"+"All"+"' WHERE "+CATEGORY+ "='"+category+"'");
        db.execSQL("DELETE FROM "+ TABLE_NAME2+ " WHERE "+CATEGORY+"='"+category+"'");
    }

    public Cursor getAllData(){
        SQLiteDatabase db= this.getWritableDatabase();
        Cursor result=db.rawQuery("SELECT * FROM "+TABLE_NAME,null);
        return result;
    }
    public Cursor getAllCategories(){
        SQLiteDatabase db= this.getWritableDatabase();
        Cursor result=db.rawQuery("SELECT * FROM "+TABLE_NAME2,null);
        return result;
    }

    public Cursor getDataFromCN(String categoria,String todo){
        SQLiteDatabase db= this.getWritableDatabase();
        Cursor result2=db.rawQuery("SELECT * FROM "+TABLE_NAME+" WHERE "+MEX+ "='"+todo+"'"+" AND "+CATEGORY+"='"+categoria+"'",null);
        return result2;
    }
    public Cursor getDataFromCategory(String categoria){
        SQLiteDatabase db= this.getWritableDatabase();
        Cursor result2=db.rawQuery("SELECT * FROM "+TABLE_NAME+" WHERE "+CATEGORY+"='"+categoria+"'",null);
        return result2;
    }

    public void updateNote(int id,String messaggio){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("UPDATE "+ TABLE_NAME+ " SET "+MEX+"='"+messaggio+"' WHERE "+ID+ "='"+id+"'");
        Log.d("querymodifica","query di modifica nota eseguita:");

    }
    public void updateImage(int id,byte[] image){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("image", image);
        db.update(TABLE_NAME, cv, "ID="+id, null);
        Log.d("querymodifica","query di modifica image eseguita:");

    }
    public void updateCategory(int id,String category){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("UPDATE "+ TABLE_NAME+ " SET "+CATEGORY+"='"+category+"' WHERE "+ID+ "='"+id+"'");
        Log.d("querymodifica","query di modifica image eseguita:");

    }
    public void deletePos(String todo,String createon,String category){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+ TABLE_NAME + " WHERE "+MEX+ "='"+todo+"'"+" AND "+DAT+"='"+createon+"' AND "+CATEGORY+"='"+category+"'");
    }
}
