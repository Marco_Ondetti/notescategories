package com.example.notescategories;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;


public class ModificaN extends AppCompatActivity {

    DatabaseHelper mydb;
    EditText TextM;
    ImageView ImageM;
    Button BottoneC;
    Button Photo;
    int id;
    static int id2;
    String todo;
    ListaNote note;
    String categoria;
    Context ModificaNContext;
    Bitmap bitmap;
    byte[] imagedata;
    private ArrayAdapter<String> listViewAdapter;
    private ArrayAdapter<String> spinnerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modifica_n);

        ModificaNContext =getApplicationContext();
        id= getIntent().getExtras().getInt("ID");
        categoria= getIntent().getExtras().getString("CATEGORIA");
        todo= getIntent().getExtras().getString("TODO");
        mydb=MainActivity.getMyDb();
        TextM =findViewById(R.id.editTextM);
        ImageM =findViewById(R.id.imageViewM);
        BottoneC =findViewById(R.id.confermaM);
        Photo =findViewById(R.id.modificaF);
        ListView lv=(ListView) findViewById(R.id.ListViewV);
        listViewAdapter=new ArrayAdapter<String>(this,R.layout.riga);
        lv.setAdapter(listViewAdapter);
        note = ListaNote.getInstance();

        displayNote(id,categoria,todo,mydb);

        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Modifica la nota");

        spinnerAdapter=new ArrayAdapter<String>(this, R.layout.riga);
        spinnerAdapter.addAll(note.getElemento());
        final Spinner sp=(Spinner) findViewById(R.id.SpinnerV);
        sp.setAdapter(spinnerAdapter);
        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    categoria= sp.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        BottoneC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mydb.updateNote(id2, TextM.getText().toString());
                mydb.updateImage(id2,imagedata);
                mydb.updateCategory(id2,categoria);

                Intent returnMain=new Intent(ModificaNContext,MainActivity.class);
                startActivity(returnMain);
                Toast.makeText(ModificaNContext, "Nota modificata con successo", Toast.LENGTH_LONG).show();
            }
        });
        Photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,100);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode,int resultCode,Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==100){
            Bitmap captureImage=(Bitmap) data.getExtras().get("data");
            ImageM.setImageBitmap(captureImage);
            ByteArrayOutputStream baos=new ByteArrayOutputStream();
            captureImage.compress(Bitmap.CompressFormat.JPEG,100,baos);
            imagedata=baos.toByteArray();
        }
    }

    public boolean onSupportNavigateUp() {
        Toast.makeText(this, "Nota non modificata", Toast.LENGTH_LONG).show();
        Intent resultIntent = new Intent(this,MainActivity.class);
        startActivity(resultIntent);
        return true;
    }


    public void displayNote(int id,String categoria,String todo,DatabaseHelper mydb2) {
        Cursor result = mydb2.getDataFromCN(categoria,todo);
        if (result.getCount() <= 0) {
        } else {
            while (result.moveToNext()) {
                String createline = result.getString(2);
                String todo2 = result.getString(1);
                String Category = result.getString(4);
                byte[] image=result.getBlob(5);
                id2= result.getInt(0);
                if(image!=null) {
                    bitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
                }
                DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                Date date = null;
                try {
                    date = format.parse(createline);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                GregorianCalendar calendar = new GregorianCalendar();
                calendar.setTime(date);
                ImageM.setImageBitmap(bitmap);
                TextM.setText(todo);
            }
        }
    }


}
