package com.example.notescategories;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class CancellaC extends AppCompatActivity {
    private Button Bottonen;
    private ListaNote note;
    private ArrayAdapter<String> spinnerAdapter;
    private String Categoria;
    DatabaseHelper mydb;
    Context CancellaCContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cancella_c);

        note = ListaNote.getInstance();
        mydb = new DatabaseHelper(this);
        Bottonen =findViewById(R.id.deleteC);
        CancellaCContext =this.getApplicationContext();
        spinnerAdapter=new ArrayAdapter<String>(this, R.layout.riga);

        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Cancella la categoria");


        spinnerAdapter.addAll(note.getElemento());
        final Spinner sp=(Spinner) findViewById(R.id.Elementi);
        sp.setAdapter(spinnerAdapter);

        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Categoria = sp.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Bottonen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                note.removeElemento(Categoria);
                Intent resultIntent = new Intent();
                resultIntent.putExtra("CATEGORY", Categoria);
                setResult(RESULT_OK, resultIntent);
                finish();
            }
        });

    }

    public boolean onSupportNavigateUp() {
        Intent resultIntent = new Intent(this,MainActivity.class);
        startActivity(resultIntent);
        return true;
    }


}
